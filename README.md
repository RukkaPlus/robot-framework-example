Tests con Robot Framework
=========================

Este proyecto es un ejemplo adaptando los tests [SAMPTD-137](https://jira.itaipu/browse/SAMPTD-137) y [NOMBRE](ENLACE) en *Robot Framework*

Instalación
-----------

Este proyecto está basado en **Python**, debe tener instalada una adaptación de **Python** en su máquina para empezar a trabajar con este proyecto 

> Asegúrese de que las variables de entorno estén [debídamente configuradas](https://tecnoloco.istocks.club/como-agregar-python-a-la-variable-path-de-windows-wiki-util/2020-10-14/)

Luego de clonar el repositorio locálmente a su máquina, abra una consola de linea de comandos en el directorio (`Shift` + `Click derecho` > *Abrir la ventana de PowerShell aquí*)

Seguido a ello, inicie un nuevo *Entorno Virtual* usando el módulo stándard `venv` y luego inícielo en su consola de linea de comandos

```powershell
python -m venv env
env\Scripts\activate.bat
```

> Si desea, también puede usar una alternativa a `venv` en su lugar, por ejemplo `virtualenv`

Ahora solo debe instalar todos los módulos del proyecto, para ello, ejecute esta otra linea en su consola de linea de comandos

```powershell
pip install -r requirements.txt

# Si lo de arriba por alguna razón llegase a fallar,
# puede intentar ejecutar el siguiente comando:
#   python -m pip install -r requirements.txt
```

> Tenga presente que cada vez que desee volver a trabajar el proyecto, tendrá que volver a iniciar el entorno virtual en su consola con el siguiente comando: `env\Scripts\activate.bat`.

> Si el directorio de su entorno virtual está corrupto o ha sido eliminado, tendrá que repetir estos pasos nuévamente

> Este proyecto está pensado para ser utilizado en entornos *MSDOS* (Windows 7/8/8.1/10/11). Si tiene pensado usarlo en un entorno *UNIX* (como GNU/Linux o MacOS) tendrá que adaptarlo a sus necesidades manuálmente

> Recuerde no compartir el directorio de su entorno virtual, porque esto puede no funcionar debídamente en otras computadoras. Aparte de ello, es una pésima práctica subirla al repositorio remoto, así que asegúrese de que no lo está subiendo por accidente en **Git**

Iniciar el test **TODO**
---------------

Para inciar los tests, ejecute el script `run.bat`. Tambien puede hacerlo desde el mismo *Explorador de archivos*

Script `run.bat`
------------------

Este script, inicia todas las pruebas del directorio `Tests` usando **Robot Framework**. Además, automáticamente inicia el *Entorno Virtual*

Script `clean.bat`
------------------

Este script, al ser ejecutado, borrará todos los archivos del directorio `Results`, donde se almacenan todos los archivos resultados (`log.html`, `report.html`, etc) de las pruebas de **Robot Framework**