*** Settings ***
Documentation   El siguiente archivo es una adaptación en Robot Framework
...             del programa https://jira.itaipu/browse/SAMPTD-86

Resource        ../Resources/Keywords.resource

*** Test Cases ***
Usuário logado com perfil MGS
    Acesso a página de login do sistema
    Preencho o campo Username com           %{USERNAME}
    Preencho o campo Senha com              %{PASSWORD}
    Clico no botão de login
    Devo estar logado com sucesso
    Eu fecho o navegador

