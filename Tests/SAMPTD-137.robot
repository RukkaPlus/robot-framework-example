*** Settings ***
Documentation   El siguiente archivo es una adaptación en Robot Framework
...             del programa https://jira.itaipu/browse/SAMPTD-137

Library         SeleniumLibrary

*** Variables ***
${URL_SAM}=     https://oshom.itaipu/SAM/Login.aspx
${URL_SMP}=     https://oshom.itaipu/SAMPlanningManagement/AnnualPlanning.aspx

# *** Test Cases ***
# Incluir Controle de Desligamento Anual - Atualização dos campos de registro
    # Given seleciono a opção para incluir um controle de desligamento anual
    # When preencho manualmente os campos <AnoDesligamento> e <TipoDesligamento>
    # And clico no botão de confirmação
    # And a mensagem de sucesso é apresentada
    # And seleciono o registro criado para visualizar seus detalhes
    # Then os campos Criado Por e Data da Criação devem estar preenchidos com o usuário quem criou o registro e a data que o registro foi criado

    # Examples:
    #     |AnoDesligamento        |TipoDesligamento       |
    #     |2022                   |Oficial                |

# *** Keywords ***
# 