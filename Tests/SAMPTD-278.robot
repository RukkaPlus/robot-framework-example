*** Settings ***
Documentation   El siguiente archivo es una adaptación en Robot Framework
...             del programa https://jira.itaipu/browse/SAMPTD-278

Resource        ../Resources/Keywords.resource

*** Test Cases ***
Ano do Desligamento "Oficial" com SSP's geradas deve existir no SMP
    Acesso o sistema SMP                                    %{USERNAME}     %{PASSWORD}
    Visualizo o regsitro com ano da programação igual a     2024
    Visualizo o regsitro com tipo da programação é igual a  Oficial
    Deve ser possível criar um novo registro de controle de desligamento anual no sistema SAM-PTD
    Eu fecho o navegador
