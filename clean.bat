@ECHO off
REM Borra todos los archivos generados por Robot Framework

ECHO Los archivos "geckodriver.log", "selenium-screenshot.png", "report.html" "log.html" "output.xml" serán borrados.
SET /p decision= "¿Está usted de acuerdo con eso? [s, n]: "

IF %decision%==s GOTO borrar
IF %decision%==S GOTO borrar
IF %decision%==n EXIT
IF %decision%==N EXIT

:borrar
CD .\Results
DEL "geckodriver-*.log" "selenium-screenshot-*.png" "report.html" "log.html" "output.xml"
CD ..\
ECHO Pulse una tecla para salir.
PAUSE > NUL
EXIT
