# Borra todos los archivos generados por Robot Framework

echo 'Los archivos "geckodriver.log", "selenium-screenshot.png", "report.html" "log.html" "output.xml" serán borrados.'
read -p "¿Está usted de acuerdo con eso? [s, n]: " decision

function borrar
{
    cd ./Results
    rm -rf "geckodriver-*.log" "selenium-screenshot-*.png" "report.html" "log.html" "output.xml"
    cd ..
}

if [[ $decision == "n" || $decision == "N" ]]
then
    borrar
fi

